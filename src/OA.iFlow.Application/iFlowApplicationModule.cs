﻿using Abp.AutoMapper;
using Abp.Modules;
using Abp.Reflection.Extensions;
using OA.iFlow.Authorization;

namespace OA.iFlow
{
    [DependsOn(
        typeof(iFlowCoreModule), 
        typeof(AbpAutoMapperModule))]
    public class iFlowApplicationModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.Authorization.Providers.Add<iFlowAuthorizationProvider>();
        }

        public override void Initialize()
        {
            var thisAssembly = typeof(iFlowApplicationModule).GetAssembly();

            IocManager.RegisterAssemblyByConvention(thisAssembly);

            Configuration.Modules.AbpAutoMapper().Configurators.Add(cfg =>
            {
                // Scan the assembly for classes which inherit from AutoMapper.Profile
                cfg.AddProfiles(thisAssembly);
            });
        }
    }
}
