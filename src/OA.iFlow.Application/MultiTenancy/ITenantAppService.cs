﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using OA.iFlow.MultiTenancy.Dto;

namespace OA.iFlow.MultiTenancy
{
    public interface ITenantAppService : IAsyncCrudAppService<TenantDto, int, PagedResultRequestDto, CreateTenantDto, TenantDto>
    {
    }
}
