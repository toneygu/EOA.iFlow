﻿using System.Threading.Tasks;
using Abp.Application.Services;
using OA.iFlow.Sessions.Dto;

namespace OA.iFlow.Sessions
{
    public interface ISessionAppService : IApplicationService
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations();
    }
}
