﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using OA.iFlow.MultiTenancy;

namespace OA.iFlow.Sessions.Dto
{
    [AutoMapFrom(typeof(Tenant))]
    public class TenantLoginInfoDto : EntityDto
    {
        public string TenancyName { get; set; }

        public string Name { get; set; }
    }
}
