﻿using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Runtime.Session;
using OA.iFlow.Configuration.Dto;

namespace OA.iFlow.Configuration
{
    [AbpAuthorize]
    public class ConfigurationAppService : iFlowAppServiceBase, IConfigurationAppService
    {
        public async Task ChangeUiTheme(ChangeUiThemeInput input)
        {
            await SettingManager.ChangeSettingForUserAsync(AbpSession.ToUserIdentifier(), AppSettingNames.UiTheme, input.Theme);
        }
    }
}
