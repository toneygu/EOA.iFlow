﻿using System.Threading.Tasks;
using OA.iFlow.Configuration.Dto;

namespace OA.iFlow.Configuration
{
    public interface IConfigurationAppService
    {
        Task ChangeUiTheme(ChangeUiThemeInput input);
    }
}
