using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using OA.iFlow.Roles.Dto;
using OA.iFlow.Users.Dto;

namespace OA.iFlow.Users
{
    public interface IUserAppService : IAsyncCrudAppService<UserDto, long, PagedResultRequestDto, CreateUserDto, UserDto>
    {
        Task<ListResultDto<RoleDto>> GetRoles();

        Task ChangeLanguage(ChangeUserLanguageDto input);
    }
}
