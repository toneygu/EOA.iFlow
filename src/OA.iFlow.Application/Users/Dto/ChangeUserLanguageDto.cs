using System.ComponentModel.DataAnnotations;

namespace OA.iFlow.Users.Dto
{
    public class ChangeUserLanguageDto
    {
        [Required]
        public string LanguageName { get; set; }
    }
}