using Microsoft.AspNetCore.Antiforgery;
using OA.iFlow.Controllers;

namespace OA.iFlow.Web.Host.Controllers
{
    public class AntiForgeryController : iFlowControllerBase
    {
        private readonly IAntiforgery _antiforgery;

        public AntiForgeryController(IAntiforgery antiforgery)
        {
            _antiforgery = antiforgery;
        }

        public void GetToken()
        {
            _antiforgery.SetCookieTokenAndHeader(HttpContext);
        }
    }
}
