﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Abp.Modules;
using Abp.Reflection.Extensions;
using OA.iFlow.Configuration;

namespace OA.iFlow.Web.Host.Startup
{
    [DependsOn(
       typeof(iFlowWebCoreModule))]
    public class iFlowWebHostModule: AbpModule
    {
        private readonly IHostingEnvironment _env;
        private readonly IConfigurationRoot _appConfiguration;

        public iFlowWebHostModule(IHostingEnvironment env)
        {
            _env = env;
            _appConfiguration = env.GetAppConfiguration();
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(iFlowWebHostModule).GetAssembly());
        }
    }
}
