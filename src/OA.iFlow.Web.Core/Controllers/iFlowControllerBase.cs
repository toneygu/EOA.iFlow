using Abp.AspNetCore.Mvc.Controllers;
using Abp.IdentityFramework;
using Microsoft.AspNetCore.Identity;

namespace OA.iFlow.Controllers
{
    public abstract class iFlowControllerBase: AbpController
    {
        protected iFlowControllerBase()
        {
            LocalizationSourceName = iFlowConsts.LocalizationSourceName;
        }

        protected void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }
    }
}
