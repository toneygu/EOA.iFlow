﻿using Abp.AutoMapper;
using OA.iFlow.Authentication.External;

namespace OA.iFlow.Models.TokenAuth
{
    [AutoMapFrom(typeof(ExternalLoginProviderInfo))]
    public class ExternalLoginProviderInfoModel
    {
        public string Name { get; set; }

        public string ClientId { get; set; }
    }
}
