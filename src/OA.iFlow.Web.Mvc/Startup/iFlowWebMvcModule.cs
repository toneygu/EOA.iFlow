﻿using Abp.Modules;
using Abp.Reflection.Extensions;
using OA.iFlow.Configuration;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;

namespace OA.iFlow.Web.Startup
{
    [DependsOn(typeof(iFlowWebCoreModule))]
    public class iFlowWebMvcModule : AbpModule
    {
        private readonly IHostingEnvironment _env;
        private readonly IConfigurationRoot _appConfiguration;

        public iFlowWebMvcModule(IHostingEnvironment env)
        {
            _env = env;
            _appConfiguration = env.GetAppConfiguration();
        }

        public override void PreInitialize()
        {
            Configuration.Navigation.Providers.Add<iFlowNavigationProvider>();
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(iFlowWebMvcModule).GetAssembly());
        }
    }
}