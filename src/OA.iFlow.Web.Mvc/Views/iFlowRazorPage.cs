﻿using Abp.AspNetCore.Mvc.Views;
using Abp.Runtime.Session;
using Microsoft.AspNetCore.Mvc.Razor.Internal;

namespace OA.iFlow.Web.Views
{
    public abstract class iFlowRazorPage<TModel> : AbpRazorPage<TModel>
    {
        [RazorInject]
        public IAbpSession AbpSession { get; set; }

        protected iFlowRazorPage()
        {
            LocalizationSourceName = iFlowConsts.LocalizationSourceName;
        }
    }
}
