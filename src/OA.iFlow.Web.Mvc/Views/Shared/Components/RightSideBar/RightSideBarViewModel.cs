﻿using OA.iFlow.Configuration.Ui;

namespace OA.iFlow.Web.Views.Shared.Components.RightSideBar
{
    public class RightSideBarViewModel
    {
        public UiThemeInfo CurrentTheme { get; set; }
    }
}
