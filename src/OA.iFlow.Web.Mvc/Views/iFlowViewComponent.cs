﻿using Abp.AspNetCore.Mvc.ViewComponents;

namespace OA.iFlow.Web.Views
{
    public abstract class iFlowViewComponent : AbpViewComponent
    {
        protected iFlowViewComponent()
        {
            LocalizationSourceName = iFlowConsts.LocalizationSourceName;
        }
    }
}