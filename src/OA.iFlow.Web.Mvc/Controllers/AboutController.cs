﻿using Abp.AspNetCore.Mvc.Authorization;
using OA.iFlow.Controllers;
using Microsoft.AspNetCore.Mvc;

namespace OA.iFlow.Web.Controllers
{
    [AbpMvcAuthorize]
    public class AboutController : iFlowControllerBase
    {
        public ActionResult Index()
        {
            return View();
        }
	}
}