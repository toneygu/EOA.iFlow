﻿using Abp.AspNetCore.Mvc.Authorization;
using OA.iFlow.Controllers;
using Microsoft.AspNetCore.Mvc;

namespace OA.iFlow.Web.Controllers
{
    [AbpMvcAuthorize]
    public class HomeController : iFlowControllerBase
    {
        public ActionResult Index()
        {
            return View();
        }
	}
}