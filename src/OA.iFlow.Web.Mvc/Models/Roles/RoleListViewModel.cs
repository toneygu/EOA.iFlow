﻿using System.Collections.Generic;
using OA.iFlow.Roles.Dto;

namespace OA.iFlow.Web.Models.Roles
{
    public class RoleListViewModel
    {
        public IReadOnlyList<RoleDto> Roles { get; set; }

        public IReadOnlyList<PermissionDto> Permissions { get; set; }
    }
}
