using System.Collections.Generic;
using OA.iFlow.Roles.Dto;
using OA.iFlow.Users.Dto;

namespace OA.iFlow.Web.Models.Users
{
    public class UserListViewModel
    {
        public IReadOnlyList<UserDto> Users { get; set; }

        public IReadOnlyList<RoleDto> Roles { get; set; }
    }
}