using System.Collections.Generic;
using System.Linq;
using OA.iFlow.Roles.Dto;
using OA.iFlow.Users.Dto;

namespace OA.iFlow.Web.Models.Users
{
    public class EditUserModalViewModel
    {
        public UserDto User { get; set; }

        public IReadOnlyList<RoleDto> Roles { get; set; }

        public bool UserIsInRole(RoleDto role)
        {
            return User.RoleNames != null && User.RoleNames.Any(r => r == role.NormalizedName);
        }
    }
}