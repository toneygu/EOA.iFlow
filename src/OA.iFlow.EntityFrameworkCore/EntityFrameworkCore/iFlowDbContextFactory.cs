﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using OA.iFlow.Configuration;
using OA.iFlow.Web;

namespace OA.iFlow.EntityFrameworkCore
{
    /* This class is needed to run "dotnet ef ..." commands from command line on development. Not used anywhere else */
    public class iFlowDbContextFactory : IDesignTimeDbContextFactory<iFlowDbContext>
    {
        public iFlowDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<iFlowDbContext>();
            var configuration = AppConfigurations.Get(WebContentDirectoryFinder.CalculateContentRootFolder());

            iFlowDbContextConfigurer.Configure(builder, configuration.GetConnectionString(iFlowConsts.ConnectionStringName));

            return new iFlowDbContext(builder.Options);
        }
    }
}
