using System.Data.Common;
using Microsoft.EntityFrameworkCore;

namespace OA.iFlow.EntityFrameworkCore
{
    public static class iFlowDbContextConfigurer
    {
        public static void Configure(DbContextOptionsBuilder<iFlowDbContext> builder, string connectionString)
        {
            builder.UseMySql(connectionString);
        }

        public static void Configure(DbContextOptionsBuilder<iFlowDbContext> builder, DbConnection connection)
        {
            builder.UseMySql(connection);
        }
    }
}
