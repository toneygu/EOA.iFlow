﻿using Microsoft.EntityFrameworkCore;
using Abp.Zero.EntityFrameworkCore;
using OA.iFlow.Authorization.Roles;
using OA.iFlow.Authorization.Users;
using OA.iFlow.MultiTenancy;

namespace OA.iFlow.EntityFrameworkCore
{
    public class iFlowDbContext : AbpZeroDbContext<Tenant, Role, User, iFlowDbContext>
    {
        /* Define an IDbSet for each entity of the application */
        
        public iFlowDbContext(DbContextOptions<iFlowDbContext> options)
            : base(options)
        {
        }
    }
}
