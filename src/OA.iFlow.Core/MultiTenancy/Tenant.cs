﻿using Abp.MultiTenancy;
using OA.iFlow.Authorization.Users;

namespace OA.iFlow.MultiTenancy
{
    public class Tenant : AbpTenant<User>
    {
        public Tenant()
        {            
        }

        public Tenant(string tenancyName, string name)
            : base(tenancyName, name)
        {
        }
    }
}
