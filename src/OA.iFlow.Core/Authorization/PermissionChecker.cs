﻿using Abp.Authorization;
using OA.iFlow.Authorization.Roles;
using OA.iFlow.Authorization.Users;

namespace OA.iFlow.Authorization
{
    public class PermissionChecker : PermissionChecker<Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {
        }
    }
}
