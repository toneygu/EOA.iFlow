﻿namespace OA.iFlow
{
    public class iFlowConsts
    {
        public const string LocalizationSourceName = "iFlow";

        public const string ConnectionStringName = "Default";

        public const bool MultiTenancyEnabled = true;
    }
}
